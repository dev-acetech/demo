<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('request_view/{id}', function () {
    return view('/Supplier_Dashboard/request_view');
});
Route::get('/Supplier_Dashboard/request_view/{id}', function () {
    return view('/Supplier_Dashboard/request_view');
});


/*Supplier Links*/
Route::get('create-invoice', 'dashboard@createInvoice');
Route::get('user-profile', 'dashboard@userProfile');
Route::get('dashboard', 'dashboard@dashboard');
Route::get('contact', 'dashboard@contact');
Route::get('response', 'dashboard@supplier_Response');
Route::get('notifications', 'dashboard@notifications');
Route::get('all-invoices', 'dashboard@supplierAllRequests');
Route::get('update-profile', 'dashboard@updateProfile');


//Route::get('/home', 'HomeController@index')->name('home');

Route::get('post_request/{id}', 'dashboard@postRequest');

Route::get('view_request', 'dashboard@viewRequest');
Route::get('view/{id}', 'dashboard@view');

//Route::get('/bankDecline', array('uses' =>'dashboard@bankDecline'));

Route::get('supplier_accept/{id}', 'dashboard@supplierAccept');
Route::get('supplier_decline/{id}', 'dashboard@supplierDecline');
Route::post('/bankDecline', array('uses' => 'dashboard@bankDecline'));

Route::post('/createInvoice', array('uses' => 'dashboard@addInvoice'));
Route::post('/placeBid', array('uses' => 'dashboard@placeBid'));