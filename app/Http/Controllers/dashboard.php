<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use auth;
use Mail;

class dashboard extends Controller
{
    public function createInvoice()
    {
        return view('/Supplier_Dashboard/create_request');

    }
    public function notifications()
    {
        return view('/Supplier_Dashboard/notifications');

    }
    public function supplier_Response()
    {
        return view('/Supplier_Dashboard/response');

    }
     public function userProfile()
    {
        return view('/user_profile');

    }
     public function dashboard()
    {
        return view('/Supplier_Dashboard/dashboard');

    }
     public function contact()
    {
        return view('/contact');

    }
    public function addInvoice(request $request)
    {
       $store_data = DB::table('request')->insertGetId(
            [
             'request_id' => 'refer_id_for_req_id',
             'user_id' => Auth::user()->id,
             'payee'=>$request->payee, 

             'payer' => $request->payer, 
             'amount_numbers' => $request->amount_numbers, 
             'amount_words' => $request->amount_words, 

             'no_of_days' => $request->no_of_days, 
             'start_payment_date' => $request->start_payment_date, 
             'end_payment_date' => $request->end_payment_date,

             'supplier_email' =>  Auth::user()->email,
             'buyer_name' => $request->buyer_name, 
             'buyers_email' => $request->buyers_email,

             'buyer_address' => $request->buyer_address,
             'bill_no' => $request->bill_no, 
             'bill_date' => $request->bill_date,

             'buyers_place' => $request->buyers_place, 
             'supplier_place'=> $request->supplier_place,
             'is_posted' => 0,

             'status' => 0, 
             'invoice_type' => 'NA', 
             'attachments' => 'files'
          ]
        );
   
      return redirect('/request_view/'.$store_data);
    }

    public function updateProfile(request $request){
        if( Auth::user()->role == "bank"){
            $update_profile = DB::table('users')->insert(
                [
                 'name' => $request->name, 
                 'email' => $request->email, 
                 'com_aadhar' => $request->com_adhaar, 
                 'city' => $request->city,
              ]
            );
        }else{
           $update_profile = DB::table('users')->insert(
                [
                 'name' => $request->name, 
                 'email' => $request->email, 
                 'gst_no' => $request->gst_no,
                 'tin_no' => $request->tin_no, 
                 'tan_no' => $request->tan_no,
                 'pan_no' => $request->pan_no,
                 'com_aadhar' => $request->com_adhaar, 
                 'city' => $request->city,
              ]
            ); 
        }

        return view('/user_profile')->with('success','Profile updated successfully!');;
        
    }
    public function postRequest($id)
    {
        if(Auth::user()->role == 'supplier'){
            $request_post =  DB::table('request')
                    ->where('id', $id)
                    ->update(['is_posted' => 1,'invoice_type' => '']);
			$get_bankers_data =  DB::table('users')
            ->where('role', 'bank')->get();
            // $data[] = $get_mail_data;
            $data['receiver_name'] = Auth::user()->name;
            Mail::send('emails.supplier_post', $data, function($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                        ->subject('Invoice Posted Succcessfully');
            });
            foreach ($get_bankers_data as $get_bankers_data){
               $data['receiver_name'] = $get_bankers_data->name;
             
                 Mail::send('emails.bank_get_post_request', $data, function($message) use($get_bankers_data) {
                    $message->to($get_bankers_data->email)->subject('You have a New Request');
                });
            }
            return view('/Supplier_Dashboard/dashboard')->with('successMsg','Invoice successfully Posted .');
        }else{
            $request_post =  DB::table('request')
            ->where('id', $id)
            ->update(['is_accepted' => 1]);
        return view('/Bank_Dashboard/dashboard')->with('success','Bill accepted successfully!');
        }

       
    }
    public function viewRequest()
    {
        return view('/Bank_Dashboard/view_all_requests');
    }
    public function view($id)
    {

      return redirect('/request_view/'.$id);
       // return view('/Bank_Dashboard/view_request');
    }

    /*Supplier all requests*/
    public function supplierAllRequests()
    {
        return view('/Supplier_Dashboard/all_requests');
    }
     public function placeBid(request $request)
    {


        $store_data = DB::table('placedbid')->insertGetId(
            [
             'req_id' => $request->req_id,
             'bidBudget' => $request->bidBudget,
             'bidRate' =>$request->bidRate,
             'user_id' => Auth::user()->id,
             'bidDate'=> $request->bidDate,
             'bank_name'=>Auth::user()->name,
             'supplier_id'=> $request->supplier_id,
             'supplier_amount' => $request->supplier_amount,
             'status'=>'bidded'
             
          ]
        );
        return view('/Bank_Dashboard/dashboard')->with('success','Bill accepted successfully!');
    }
    
     public function bankDecline(request $request)
    {
        echo "test";
        exit;
        // supplier_id
        // supplier_amount
        // req_id
    }

     public function supplierDecline($id)
    {
        $accept_offer = DB::table('placedbid')->where(['req_id' => $id,
                                                     'supplier_id' => Auth::user()->id
                                                 ])->update(['status' => 'declined']);

      return view('/Supplier_Dashboard/dashboard')->with('success','Bill declined successfully!');
    }
    public function supplierAccept($id)
    {
       $accept_offer = DB::table('placedbid')->where(['req_id' => $id,
                                                     'supplier_id' => Auth::user()->id
                                                 ])->update(['status' => 'accepted']);

      return view('/Supplier_Dashboard/dashboard')->with('success','Bill accepted successfully!');

    }
    
    
}
