<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class request extends Model
{
     use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_fname', 'supplier_lname', 'request_id',
    ];
}

