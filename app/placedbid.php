<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class placedbid extends Model
{
     use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'req_id', 'bidBudget', 'bidRate','user_id','bidDate','bank_name',
    ];
}
