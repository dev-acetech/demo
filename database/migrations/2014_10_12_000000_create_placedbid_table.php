<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacedbidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('placedbid', function (Blueprint $table) {
            $table->increments('id');
            $table->string('req_id');
            $table->string('bidBudget');
            $table->string('bidRate');
            $table->string('user_id');
            $table->string('bidDate');
            $table->string('bank_name');
            $table->string('status');
            $table->string('supplier_id');
            $table->string('supplier_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('placedbid');
    }
}
