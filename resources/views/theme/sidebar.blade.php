<div class="sidebar" data-color="purple" data-background-color="white" data-image="/img/sidebar-1.jpg">
            
            <div class="logo">
                <a href="http://www.klathrate.com" class="simple-text logo-normal">
                    Invoices
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="nav-item active ">
                        <a class="nav-link" href="/dashboard">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="/user-profile">
                            <i class="material-icons">person</i>
                            <p>User Profile</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                         @if(Auth::user()->role == 'supplier')
                        <a class="nav-link" href="/create-invoice">
                            <i class="material-icons">content_paste</i>
                            <p>Create Invoice</p>
                        </a>
                         @else
                         <a class="nav-link" href="view_invoices">
                            <i class="material-icons">content_paste</i>
                            <p>View Invoice</p>
                        </a>
                         @endif
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="/notifications">
                            <i class="material-icons">notifications</i>
                            <p>Notifications</p>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="/response">
                            <i class="material-icons">bubble_chart</i>
                            <p>Response</p>
                        </a>
                    </li>
                     @if(Auth::user()->role == 'supplier')
                     <li class="nav-item ">
                        <a class="nav-link" href="/all-invoices">
                            <i class="material-icons">library_books</i>
                            <p>My Invoices</p>
                        </a>
                    </li>
                    @endif
                    <li class="nav-item ">
                        <a class="nav-link" href="/contact">
                            <i class="material-icons">location_ons</i>
                            <p>Contact</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>