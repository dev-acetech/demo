@extends('theme.default')


@section('content')
@if(!empty($successMsg))
  <div class="alert alert-success"> {{ $successMsg }}</div>
@endif
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="container">
                            <div class="card">
                                <div class="card-header card-header-primary">
                                    <h4 class="card-title">Edit Profile</h4>
                                    <p class="card-category">Complete your profile</p>
                                </div>
                                <div class="card-body">
                                    <form method="POST" action="/update-profile">
                                             @csrf
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Name</label>
                                                    <input type="text" name="name" class="form-control" value="{{Auth::user()->name}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Email</label>
                                                    <input type="email" name="email" class="form-control" value="{{Auth::user()->email}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">GST No.</label>
                                                    <input type="text" name="gst_no" class="form-control" value="{{Auth::user()->gst_no}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Tin No.</label>
                                                    <input type="text" name="tin_no" class="form-control" value="{{Auth::user()->tin_no}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Tan No.</label>
                                                    <input type="text" name="tan_no" class="form-control" value="{{Auth::user()->tan_no}}">
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">PAN No.</label>
                                                    <input type="text" name="pan_no" class="form-control" value="{{Auth::user()->pan_no}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">Company Adhaar</label>
                                                    <input type="text" name="com_adhaar" class="form-control" value="{{Auth::user()->com_aadhar}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="bmd-label-floating">City</label>
                                                    <input type="text" name="city"  class="form-control" value="{{Auth::user()->city}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>About Me</label>
                                                    <div class="form-group">
                                                        <label class="bmd-label-floating"> Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</label>
                                                        <textarea class="form-control" rows="5"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
         
@endsection