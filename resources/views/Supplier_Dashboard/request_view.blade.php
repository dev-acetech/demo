@extends('theme.default')


@section('content')
<!-- <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Invoice View</h1>
    </div>
</div> -->

<?php  
$id = \Request::segment(2);
$request_data = DB::table('request')
                ->where('id','=',$id)
                ->first();
if($bid_data = DB::table('placedbid')
                ->where(['req_id'=>$id,'user_id'=>Auth::user()->id])
                ->get()){
}


?>
               
<div class="container request_view well ">
        <div class="row">
            <div class="col-md-12"><h3 class="center highlight"><b>Bill of Exchange</b></h3></div>
            <div class="col-md-4">
                <div class="form-group">
	                <label class="normal_color">{{$request_data->supplier_place}},</label>
                    <label class="normal_color"> {{date('d-m-Y', strtotime($request_data->created_at))}}</label>
            	</div>
            </div>
         <div class="col-md-4">
                <div class="form-group">
	                <label class="highlight">Amount</label>
                    <label class="normal_color">IN Rs.{{$request_data->amount_numbers}}/-</label>
            	</div>
    	</div>
    	 <div class="col-md-4">
            <div class="form-group">
	                <label class="highlight">At</label>
                    <label class="normal_color">{{$request_data->no_of_days}} days after sight</label>
            	</div>
    	</div>
        <div class="col-md-8">
                <div class="form-group">
	                <label class="highlight">Pay against this Sole Bill of Exchange to the order of</label>
            	</div>
    	</div>
    	
        <div class="col-md-4">
                <div class="form-group">
	                <label class="normal_color">Ourselves</label>
            	</div>
    	</div>
    	 <div class="col-md-6">
    	</div>
        <div class="col-md-12"></div>
        <div class="col-md-3">
                <div class="form-group">
	                <label class="highlight">the sum of</label>
            	</div>
    	</div>
    	 <div class="col-md-9">
                <div class="form-group">
	                <label class="normal_color">In Rs. {{$request_data->amount_words}}</label>
            	</div>
    	</div>
        <div class="col-md-3">
                <div class="form-group">
	                <label class="highlight">for value</label>
            	</div>
    	</div>
    	 <div class="col-md-9">
                <div class="form-group">
	                <label class="normal_color">Received</label>
            	</div>
    	</div>
        <div class="col-md-6">
                <div class="form-group">
	                <label class="highlight">To:</label>
                    <p>{{$request_data->buyer_address}}</p>
                    <p>{{$request_data->buyer_name}}</p>
                     <p>{{$request_data->buyers_email}}</p>
            	</div>
    	</div>
    	 <div class="col-md-6 center">
                <div class="form-group">
	                <label class="highlight">For and on behalf of:</label>
                    <p>{{$request_data->payee}}</p>
                    <p>{{$request_data->supplier_place}}</p>
            	</div>
    	</div>

        </div>

        <div class="row">
        
        
       
        </div>
</div>

<div class="container request_view">
    <div class="row">
    <div class="col-md-6 left">
        <div class="form-group">
            <a href="#" ><form action="/bankDecline" method="post" enctype="multipart/form-data" name="decline">
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
             <input class="form-control bidBudget" name="supplier_id" id="supplier_id" type="hidden" value="{{$request_data->user_id}}" >
             <input class="form-control bidBudget" name="supplier_amount" id="supplier_amount" type="hidden" value="{{$request_data->amount_numbers}}" >
             <input class="form-control bidBudget" name="req_id" id="req_id" type="hidden" value="{{$request_data->id}}" >
                <button type="submit" class="btn btn-danger">Decline</button>
            </form></a>
        </div>
    </div>
    <div class="col-md-6 right">
        <div class="form-group">
            @if(Auth::user()->role == 'supplier')
                @if($request_data->is_posted == 0)
                <a href="#" class="btn btn-primary" type="button"  data-toggle="modal" data-target="#myModal5">Post</a>
                @else
                 <a href="#" disabled class="btn btn-primary">Already Posted</a>
                @endif
            @else
                @if($request_data->is_accepted == 0)
                     @if(count($bid_data) > 0)
                     <a href="#" class="btn btn-primary" type="button" disabled data-toggle="modal" data-target="#myModal4">Already Bided</a>
                     @else
                     <a href="#" class="btn btn-primary" type="button"  data-toggle="modal" data-target="#myModal4">Bid</a>
                     @endif
                @else
                 <a href="#" disabled class="btn btn-primary">Already Bided</a>
                @endif
            @endif
        </div>
    </div>
    </div>
</div><div id="myModal5" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #ececec;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title bidtitle">Post Invoice</h4>
            </div>
            <div class="modal-body row">
               
                <div class="modal-footer" style="text-align: center;">
                 <form  method="post" action="/placeBid" enctype="multipart/form-data" name="placeBid">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                      <input type="submit" id="placeBid" class="btn btn-success" value="Open Bid">
                </form>
                <form  method="post" action="/placeBid" enctype="multipart/form-data" name="placeBid">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                      <input type="submit" id="placeBid" class="btn btn-success" value="Close Bid">
                </form>
                </div>
             
        </div>
        </div>
    </div>
</div>
<div id="myModal4" class="modal fade " role="dialog" style="padding-right: 5px;" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #ececec;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title bidtitle">Place Bid</h4>
            </div>
            <div class="modal-body row">
                <div class="col-md-12 dynamicData">
                    <div class="">
                        <div class="col-md-4">Supplier Name :</div>
                        <div class="col-md-8 "><input class="form-control" value="{{$request_data->payee}}" disabled="" type="text"></div>
                    </div>
                    <div class="">
                        <div class="col-md-4">Amount :</div>
                        <div class="col-md-8 "><input class="form-control" value="{{$request_data->amount_numbers}} /-" disabled="" type="text"></div>
                    </div>
                    <div class="">
                        <div class="col-md-4">No. of Days :</div>
                        <div class="col-md-8 "><input class="form-control" value="{{$request_data->no_of_days}}" disabled="" type="text"></div>
                    </div>
                     <div class="">
                        <div class="col-md-4">Date :</div>
                        <div class="col-md-8"><input class="form-control" value="" disabled="" type="text"></div>
                    </div>
                    <div class="">
                        <div class="col-md-4">City :</div>
                        <div class="col-md-8"><textarea disabled="" type="text" class="form-control">{{$request_data->supplier_place}}</textarea></div>
                    </div>
                    <div class="col-md-12"><hr></div>
                </div> 
            <form  method="post" action="/placeBid" enctype="multipart/form-data" name="placeBid">
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <div class="col-md-12">
                    <div class="col-md-4">Amount :</div>
                    <div class="col-md-8"><input class="form-control bidBudget" name="bidBudget" id="bidBudget" type="text" placeholder="Amount" required="required"></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">Rate :</div>
                    <div class="col-md-8"><input class="form-control bidBudget" name="bidRate" id="bidRate" type="text" placeholder="Rate" required="required"></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">Date :</div>
                    <div class="col-md-8"><input class="form-control bidBudget" name="bidDate" id="bidDate" type="date" required="required" ></div>
                    <input class="form-control bidBudget" name="req_id" id="req_id" type="hidden" value="{{$request_data->id}}" >
                    <input class="form-control bidBudget" name="supplier_id" id="supplier_id" type="hidden" value="{{$request_data->user_id}}" >
                    <input class="form-control bidBudget" name="supplier_amount" id="supplier_amount" type="hidden" value="{{$request_data->amount_numbers}}" > 
                </div> 
            <div class="modal-footer"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                  @if(count($bid_data) > 0)
                  <input type="submit" id="placeBid" disabled="disabled" class="btn btn-success" value="Already Bided">
                  @else
                  <input type="submit" id="placeBid" class="btn btn-success" value="Bid">
                  @endif
                <button type="button" class="btn btn-default btnCloseModal" data-dismiss="modal">Close</button>
            </div>
        </form>
        </div>
        </div>
    </div>
</div>

@endsection