@extends('theme.default')


@section('content')

<div class="row">

    <div class="col-lg-12">

        <h1 class="page-header">Request View</h1>

    </div>

    <!-- /.col-lg-12 -->

</div>

<!-- /.row -->

<!------ Include the above in your HEAD tag ---------->

<div class="container request_view well ">
        <div class="row">
            <div class="col-md-12"><h3 class="center highlight"><b>Bill of Exchange</b></h3></div>
            <div class="col-md-4">
                <div class="form-group">
	                <label>London,</label>
                    <label> 31 January 2000</label>
            	</div>
            </div>
         <div class="col-md-4">
                <div class="form-group">
	                <label class="highlight">Amount</label>
                    <label>IN Rs. 10,000/-</label>
            	</div>
    	</div>
    	 <div class="col-md-4">
            <div class="form-group">
	                <label class="highlight">At</label>
                    <label>60 days after sight</label>
            	</div>
    	</div>
        <div class="col-md-8">
                <div class="form-group">
	                <label class="highlight">Pay against this Sole Bill of Exchange to the order of</label>
            	</div>
    	</div>
    	
        <div class="col-md-4">
                <div class="form-group">
	                <label>Ourselves</label>
            	</div>
    	</div>
    	 <div class="col-md-6">
    	</div>
        <div class="col-md-12"></div>
        <div class="col-md-3">
                <div class="form-group">
	                <label class="highlight">the sum of</label>
            	</div>
    	</div>
    	 <div class="col-md-9">
                <div class="form-group">
	                <label>US Dollars Two hundred and fifty thousand</label>
            	</div>
    	</div>
        <div class="col-md-3">
                <div class="form-group">
	                <label class="highlight">for value</label>
            	</div>
    	</div>
    	 <div class="col-md-9">
                <div class="form-group">
	                <label>Received</label>
            	</div>
    	</div>
        <div class="col-md-6">
                <div class="form-group">
	                <label class="highlight">To:</label>
                    <p>Singapore Import Banking Company
                    <br>Bank Street
                    <br>Singapore</p>
                    <p>Drawn under UK Export Banking
                    <br>Company Ltd, Documentory Credit
                    <br>No.12345, Dated 29 September 1999</p>
            	</div>
    	</div>
    	 <div class="col-md-6 center">
                <div class="form-group">
	                <label class="highlight">For and on behalf of:</label>
                    <p>UK Export Company Ltd</p>
                    <br>
                    <h3></h3>
                    <p>James Smith, Director</p>
            	</div>
    	</div>

        </div>

        <div class="row">
        
        
       
        </div>
</div>

<div class="container request_view">
    <div class="row">
    <div class="col-md-6 left">
        <div class="form-group">
            <a href="/home" class="btn btn-danger">Decline</a>
        </div>
    </div>
    <div class="col-md-6 right">
        <div class="form-group">
            <a href="/post_request" class="btn btn-primary">Accept</a>
        </div>
    </div>
    </div>
</div>

@endsection